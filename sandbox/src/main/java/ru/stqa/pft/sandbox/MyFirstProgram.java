package ru.stqa.pft.sandbox;

public class MyFirstProgram {
    public static void main(String[] args) {
        System.out.println("Ziga 1488");
        double a = 5;
        double b = 6;
        double c = 7;
        System.out.println("Площадь прямоугольника со сторонами " + a + " и " + b + " = " + area(a,b));
        System.out.println("Площадь прямоугольника со стороной " + c + " = " + area(c));
    }

    public static double area (double l) {
        return l * l;
    }
    public static double area (double a, double b) {
        return a * b;
    }
}
