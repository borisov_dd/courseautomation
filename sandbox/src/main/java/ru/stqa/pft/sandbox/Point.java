package ru.stqa.pft.sandbox;


public class Point {
    double x;
    double y;
    public Point (double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static void main(String[] args) {
            Point p1 = new Point(3.0,4.0);
            Point p2 = new Point(5.0,6.0);
            distance(p1, p2);
    }
    public static double distance(Point p1, Point p2) {
        double sqrt = Math.sqrt(Math.pow((p1.x - p2.x),2) + Math.pow((p1.y - p2.y),2));
        System.out.println(sqrt);
        return sqrt;
    }
}







