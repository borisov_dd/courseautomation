package ru.stqa.pft.sandbox;

public class Main {
    static Point p1 = new Point(2.3,4.5);
    static Point p2 = new Point(5.3,6.5);

    public static void main(String[] args) {
        System.out.println(Point.distance(p1,p2));
    }
}
