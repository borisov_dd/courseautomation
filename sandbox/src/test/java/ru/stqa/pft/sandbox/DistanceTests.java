package ru.stqa.pft.sandbox;


import org.testng.Assert;
import org.testng.annotations.Test;

public class DistanceTests {

    @Test
    public void testDistanceSuccess() {
        Point p1 = new Point(4,6);
        Point p2 = new Point(6,4);
        Assert.assertEquals(Point.distance(p1,p2),2.8284271247461903);

    }
    @Test
    public void testDistanceFailed() {
        Point p1 = new Point(4,6);
        Point p2 = new Point(6,4);
        Assert.assertNotNull(Point.distance(p1,p2));
        assert Point.distance(p1,p2) != 3.8284271247461903;

    }
}
